﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using Novacode;

namespace WordGenerator
{
    internal class Program
    {
        private static void Main()
        {
            try
            {
                using (var doc = DocX.Load(ConfigurationManager.AppSettings["TemplateFolder"]))
                {
                    var table = doc.Tables.FirstOrDefault(t => t.TableCaption == "FOLHA_DE_PONTO");

                    if (table == null) throw new NoNullAllowedException(nameof(table));
                    if (table.RowCount <= 0) return;

                    var rnd = new Random();
                    var minutesList = RandomMinutes(0, 5);
                    var inHour = "09";
                    var outHour = "18";

                    var startIndex = table.Rows.FindIndex(r => r.Cells[0].Xml.Value.Trim() == "1");
                    var finalIndex = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month) + startIndex;

                    for (var index = startIndex; index < finalIndex; index++)
                    {
                        if (table.Rows[index].Cells[1].Xml.Value.Length >= 2) continue;

                        var inMinute = minutesList.ElementAt(rnd.Next(0, minutesList.Count));

                        var increaseMinute = rnd.Next(0, 15); // Generates a random increase number for the leave time.
                        var outMinute = Convert.ToInt16(inMinute) + increaseMinute;

                        var inTime = $"{inHour}:{inMinute.FormatMinute()}";
                        var outTime = $"{outHour}:{outMinute.FormatMinute()}";

                        AddItemToRow(table.Rows[index].Cells[1], inTime);
                        AddItemToRow(table.Rows[index].Cells[3], outTime);
                    }

                    doc.SaveAs(ConfigurationManager.AppSettings["OutputFolder"]);

                    Console.WriteLine($"Arquivo criado com sucesso!");
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine($"Error:\n{exception.Message}");
            }

            Console.ReadKey();
        }

        private static IEnumerable<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                .Select(day => new DateTime(year, month, day))
                .Where(dt => dt.DayOfWeek != DayOfWeek.Sunday &&
                             dt.DayOfWeek != DayOfWeek.Saturday);
        }

        private static void AddItemToRow(Container cell, string hour)
        {
            cell.RemoveParagraph(cell.Paragraphs.FirstOrDefault());
            cell.InsertParagraph(hour);
            foreach (var paragraph in cell.Paragraphs)
            {
                paragraph.Alignment = Alignment.center;
            }
        }

        public static List<string> RandomMinutes(int startMinute, int endMinute)
        {
            var rnd = new Random();

            var minutes = new List<string>();

            for (var i = 0; i < 31; i++)
            {
                var minute = rnd.Next(startMinute, endMinute);                                
                
                minutes.Add(minute.ToString());
            }

            return minutes;
        }        
    }

    public static class Extensions {

        public static string FormatMinute(this object minute)
        {
            return minute.ToString().PadLeft(2, '0');
        }
    }
}